﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Reduxion;

namespace ReduxionExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var reducer = new CombinedReducer<State>(new Dictionary<string, ReducerBase>() {
                { "User", new UserReducer() },
                { "Config", new Config() }
            });
            var store = new Store<State>(reducer);

            Config.SetEnvironment.Reduced += OnSetEnvironment;
            Config.SetLimit.Reduced += OnLimitChanged;
            Config.IncreaseLimit.Reduced += OnLimitChanged;
            Config.SetLimit.WillReduce += OnLimitWillChange;
            Config.IncreaseLimit.WillReduce += OnLimitWillChange;

            Console.WriteLine("Initial store state:");
            Console.WriteLine(store.State);

            Console.WriteLine("Setting configuration's environment:");
            store.Dispatch(Config.SetEnvironment, Environment.Production);
            Console.WriteLine(store.State);

            Console.WriteLine("Setting configuration's limit:");
            store.Dispatch(Config.SetLimit, 11);
            Console.WriteLine(store.State);

            Console.WriteLine("Increasing configuration's limit:");
            store.Dispatch(Config.IncreaseLimit);
            Console.WriteLine(store.State);

            Console.WriteLine("Attempting to increase configuration's limit past the max of 12:");
            store.Dispatch(Config.IncreaseLimit);
            Console.WriteLine(store.State);

            Console.WriteLine("Setting user's name:");
            store.Dispatch(UserActions.SetName("Frodo Baggins"));
            Console.WriteLine(store.State);

            Console.WriteLine("Setting user's email:");
            store.Dispatch(UserActions.SetEmail("frodo.baggins@middle.earth"));
            Console.WriteLine(store.State);

            Console.WriteLine("Increase user's age:");
            store.Dispatch(UserActions.IncreaseAge());
            Console.WriteLine(store.State);

            SerializationExample(store);

            Config.SetEnvironment.Reduced -= OnSetEnvironment;
            Config.SetLimit.Reduced -= OnLimitChanged;
            Config.IncreaseLimit.Reduced -= OnLimitChanged;
            Config.SetLimit.WillReduce -= OnLimitWillChange;
            Config.IncreaseLimit.WillReduce -= OnLimitWillChange;
        }

        private static void OnLimitWillChange(WillReduceEventArgs<Config.State> args)
        {
            if (args.State.Limit == 12)
            {
                args.Cancel = true;
                return;
            }
        }

        private static void OnSetEnvironment(Config.State configState)
        {
            Console.WriteLine($"OnEnvironmentChanged: {configState.Environment}.");
        }

        private static void OnLimitChanged(Config.State configState)
        {
            Console.WriteLine($"OnLimitChanged: {configState.Limit}.");
        }

        private static void SerializationExample(Store<State> store)
        {
            var supportedActionTypeConfigs = new Dictionary<string, (Type Type, System.Action<Store<State>, object> Dispatch)>()
            {
                { typeof(string).Name, (typeof(Reduxion.Action<string>), (s, o) => s.Dispatch((Reduxion.Action<string>)o) ) }
            };

            Console.WriteLine("Serialization example:");

            var setNameAction = UserActions.SetName("Peregrin Took");
            var serializedAction = SerializedAction.Create(setNameAction);
            var serializedActionJson = JsonConvert.SerializeObject(serializedAction);
            Console.WriteLine("Serialized JSON:");
            Console.WriteLine(serializedActionJson);

            var deserializedAction = JsonConvert.DeserializeObject<SerializedAction>(serializedActionJson);
            var actionTypeConfig = supportedActionTypeConfigs[deserializedAction.Type];
            var deserializedStringAction = JsonConvert.DeserializeObject(deserializedAction.Action, actionTypeConfig.Type);

            Console.WriteLine("Dispatching deserialized action:");
            actionTypeConfig.Dispatch(store, deserializedStringAction);
            Console.WriteLine(store.State);
        }
    }
}
