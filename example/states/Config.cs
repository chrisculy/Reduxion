using Reduxion;

namespace ReduxionExample
{
    public class Config : Reducer<Config.State>
    {
        public class State
        {
            public Environment Environment { get; set; }
            public int Limit { get; set; }

            public override string ToString() => $"{{ Environment: {Environment}, Limit: {Limit} }}";
        }

        public static ActionCreator<State, System.Enum, Environment> SetEnvironment = ActionCreator<State, System.Enum, Environment>.Create();
        public static ActionCreator<State, object, int> SetLimit = ActionCreator<State, object, int>.Create();
        public static ActionCreator<State> IncreaseLimit = ActionCreator<State>.Create();
        
        public override State InitialState => new State()
        {
            Environment = Environment.Development,
            Limit = 10
        };

        public State Reduce(State state, IAction<Environment> action)
        {
            if (action.Name == SetEnvironment.Name)
            {
                state.Environment = action.Payload;
            }

            return state;
        }

        public State Reduce(State state, IAction<int> action)
        {
            if (action.Name == SetLimit.Name)
            {
                state.Limit = action.Payload;
            }

            return state;
        }

        public State Reduce(State state, IAction action)
        {
            if (action.Name == IncreaseLimit.Name)
            {
                state.Limit++;
            }

            return state;
        }
    }
}