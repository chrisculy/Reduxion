namespace ReduxionExample
{
    public class UserState
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return $"{{ Name: {Name}, Email: {Email}, Age: {Age} }}";
        }
    }
}