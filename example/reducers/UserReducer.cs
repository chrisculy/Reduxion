using Reduxion;

namespace ReduxionExample
{
    public class UserReducer : Reducer<UserState>
    {
        public override UserState InitialState => new UserState()
        {
            Name = "",
            Email = "",
            Age = 0,
        };

        public UserState Reduce(UserState state, IAction<string> action)
        {
            switch (action.Name)
            {
                case UserActions.SET_NAME:
                    state.Name = action.Payload;
                    break;
                case UserActions.SET_EMAIL:
                    state.Email = action.Payload;
                    break;
            }

            return state;
		}

		public UserState Reduce(UserState state, IAction action)
		{
			if (action.Name == UserActions.INCREASE_AGE)
			{
				state.Age++;
			}

			return state;
		}
	}
}