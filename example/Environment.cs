namespace ReduxionExample
{
    public enum Environment
    {
        Development,
        Staging,
        Production
    }
}