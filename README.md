# Reduxion

![Build Status](https://gitlab.com/chrisculy/Reduxion/badges/master/pipeline.svg?style=flat-square)
![Test Coverage](https://gitlab.com/chrisculy/Reduxion/badges/master/coverage.svg?style=flat-square)

Reduxion is a C# library inspired by Redux for managing application state.

## Example Usage

To begin setting up your application's state with Reduxion, create a class that will contain all the logic for a subsection of your application state. This class will contain an inner type to define the state, the actions that operate on that state, and will implement `Reducer<T>` (where `T` is the state type) with the various reduce methods that will handle the actions.

### Main class that implements `Reducer<T>`

```cs
namespace Example
{
    public class Config : Reducer<Config.State>
    {
        
```

### Class that defines the state

```cs
        public class State
        {
            public Environment Environment { get; set; }
            public int Limit { get; set; }

            public override string ToString() => $"{{ Environment: {Environment}, Limit: {Limit} }}";
        }
```

### Creating actions

```cs
        public static ActionCreator<State, System.Enum, Environment> SetEnvironment = ActionCreator<State, System.Enum, Environment>.Create();
        public static ActionCreator<State, object, int> SetLimit = ActionCreator<State, object, int>.Create();
        public static ActionCreator<State> IncreaseLimit = ActionCreator<State>.Create();
```

### Implement `Reducer<T>` with inital state and reduce methods

```cs
        public override State InitialState => new State()
        {
            Environment = Environment.Development,
            Limit = 10
        };

        public State Reduce(State state, IAction<Environment> action)
        {
            if (action.Name == SetEnvironment.Name)
            {
                state.Environment = action.Payload;
            }

            return state;
        }

        public State Reduce(State state, IAction<int> action)
        {
            if (action.Name == SetLimit.Name)
            {
                state.Limit = action.Payload;
            }

            return state;
        }

        public State Reduce(State state, IAction action)
        {
            if (action.Name == IncreaseLimit.Name)
            {
                state.Limit++;
            }

            return state;
        }
    }
}
```

Next, define your full application state (made up of all the substates like the one that was just defined).

```cs
namespace Example
{
    public class State
    {
        ...
        public Config.State Config { get; set; }
    }
}
```

Then, in your setup for your application, you would create the reducer and the store at the top level and use `Dispatch` on the store object to send actions.

```cs
var reducer = new CombinedReducer<State>(new Dictionary<string, ReducerBase>() {
    ...
    { "Config", new Config() }
});
var store = new Store<State>(reducer);

...
store.Dispatch(Config.SetEnvironment, Environment.Production);
...
store.Dispatch(Config.SetLimit, 11);
...
store.Dispatch(Config.IncreaseLimit);
...
```

### Action reduction events

There are two events on the `ActionCreator` class that allow you to be notified when an action is about to be reduced (`WillReduce`) and when an action has been reduced (`Reduced`). The `WillReduce` event has an `args` parameter with a `Cancel` property that can be set to `true` in your callback to prevent the action from being reduced.

#### Subscribe to events

```cs
            Config.SetEnvironment.Reduced += OnSetEnvironment;
            Config.SetLimit.Reduced += OnLimitChanged;
            Config.IncreaseLimit.Reduced += OnLimitChanged;
            Config.SetLimit.WillReduce += OnLimitWillChange;
            Config.IncreaseLimit.WillReduce += OnLimitWillChange;

```

#### Implement callbacks

```cs
        private static void OnLimitWillChange(WillReduceEventArgs<Config.State> args)
        {
            if (args.State.Limit == 12)
            {
                args.Cancel = true;
                return;
            }
        }

        private static void OnSetEnvironment(Config.State configState)
        {
            Console.WriteLine($"OnEnvironmentChanged: {configState.Environment}.");
        }

        private static void OnLimitChanged(Config.State configState)
        {
            Console.WriteLine($"OnLimitChanged: {configState.Limit}.");
        }

```

#### Unsubscribe from events

```cs
            Config.SetEnvironment.Reduced -= OnSetEnvironment;
            Config.SetLimit.Reduced -= OnLimitChanged;
            Config.IncreaseLimit.Reduced -= OnLimitChanged;
            Config.SetLimit.WillReduce -= OnLimitWillChange;
            Config.IncreaseLimit.WillReduce -= OnLimitWillChange;

```


## License

Reduxion is licensed under the [MIT license](LICENSE.md).
