using System.Diagnostics;

namespace Reduxion
{
    public static class ActionUtility
    {
        internal static string GetName(string callingMemberName)
        {
            var actionMethod = new StackTrace().GetFrame(2).GetMethod();
            return $"{actionMethod.DeclaringType}/{callingMemberName}";
        }
    }
}