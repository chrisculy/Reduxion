using System.Runtime.CompilerServices;

namespace Reduxion
{
    public class WillReduceEventArgs<TState> where TState : new()
    {
        public WillReduceEventArgs(TState state)
        {
            State = state;
        }

        public TState State { get; }
        public bool Cancel { get; set; }
    }

    public class ActionCreator<TState> where TState : new()
    {
        public static ActionCreator<TState> Create([CallerMemberName] string memberName = "") => new ActionCreator<TState>(ActionUtility.GetName(memberName));

        public string Name { get; }
        public event System.Action<WillReduceEventArgs<TState>> WillReduce;
        public event System.Action<TState> Reduced;

        private ActionCreator(string name)
        {
            Name = name;
        }

        internal Action Fire()
        {
            return Action.Create(Name);
        }

        internal void OnWillReduce(WillReduceEventArgs<TState> args) => WillReduce?.Invoke(args);

        internal void OnReduced(TState state) => Reduced?.Invoke(state);
    }

    public class ActionCreator<TState, T, U> 
        where TState : new()
        where T : class
    {
        public static ActionCreator<TState, T, U> Create([CallerMemberName] string memberName = "") => new ActionCreator<TState, T, U>(ActionUtility.GetName(memberName));

        public string Name { get; }
        public event System.Action<WillReduceEventArgs<TState>> WillReduce;
        public event System.Action<TState> Reduced;

        private ActionCreator(string name)
        {
            Name = name;
        }

        internal Action<T> Fire(U payload)
        {
            return Action<T>.Create(Name, payload);
        }

        internal void OnWillReduce(WillReduceEventArgs<TState> args) => WillReduce?.Invoke(args);

        internal void OnReduced(TState state) => Reduced?.Invoke(state);
    }
}