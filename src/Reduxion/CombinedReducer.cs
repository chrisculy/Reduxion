using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Reduxion
{
    public class CombinedReducer<T> : Reducer<T> where T : new()
    {
        public CombinedReducer(Dictionary<string, ReducerBase> reducers)
        {
            var stateProperties = typeof(T).GetProperties();
            var statePropertyNames = stateProperties.Select(stateProperty => stateProperty.Name).ToList();
            var missingProperties = statePropertyNames.Except(reducers.Keys.ToList()).ToList();
            if (missingProperties.Count != 0)
            {
                throw new ArgumentException($"Expected a reducer for the following properties of '{typeof(T).Name}': '{string.Join(", ", missingProperties)}'.");
            }

            var extraProperties = reducers.Keys.Except(statePropertyNames).ToList();
            if (extraProperties.Count != 0)
            {
                throw new ArgumentException($"Encountered reducers for unrecognized properties of '{typeof(T).Name}': '{string.Join(", ", extraProperties)}'.");
            }

            Type reducerGenericType = typeof(Reducer<>);
            m_reducerConfigs = reducers.Select(reducer =>
                {
                    var reducerConcreteType = reducerGenericType.MakeGenericType(reducer.Value.StateType);

                    return new ReducerConfig(reducer.Value, stateProperties.First(stateProperty => stateProperty.Name == reducer.Key), 
                            reducerConcreteType.GetProperty(c_initialStatePropertyName),
                            reducerConcreteType.GetMethods().FirstOrDefault(method => IsGenericReduceMethod(method, reducer.Value.StateType)),
                            reducer.Value.GetType().GetMethods().FirstOrDefault(method => IsEmptyPayloadReduceMethod(method, reducer.Value.StateType)));
                }).ToList();
        }

        public override T InitialState
        {
            get
            {
                var state = new T();
                foreach (var reducerConfig in m_reducerConfigs)
                {
                    reducerConfig.StateProperty.SetValue(state, reducerConfig.ReducerInitialStateProperty.GetValue(reducerConfig.Reducer));
                }

                return state;
            }
        }

        public override T Reduce(T state, Action action)
        {
            foreach (var reducerConfig in m_reducerConfigs.Where(r => r.ReducerEmptyPayloadReduceMethod != null))
            {
                var substate = reducerConfig.StateProperty.GetValue(state);
                var reduceMethod = reducerConfig.ReducerEmptyPayloadReduceMethod;
                var newSubstate = reduceMethod.Invoke(reducerConfig.Reducer, new[] { substate, action });
                reducerConfig.StateProperty.SetValue(state, newSubstate);
            }

            return state;
        }

        public override T Reduce<U>(T state, Action<U> action)
        {
            var actionType = action.OriginalType ?? typeof(U);
            foreach (var reducerConfig in m_reducerConfigs.Where(r => r.Reducer.SupportsActionType(actionType)))
            {
                var substate = reducerConfig.StateProperty.GetValue(state);
                var reduceMethod = reducerConfig.ReducerReduceMethod.MakeGenericMethod(typeof(U));
                var newSubstate = reduceMethod.Invoke(reducerConfig.Reducer, new[] { substate, action });
                reducerConfig.StateProperty.SetValue(state, newSubstate);
            }

            return state;
        }

		public override bool SupportsActionType(Type actionType)
		{
			return true;
		}

		private class ReducerConfig
        {
            public ReducerConfig(ReducerBase reducer, PropertyInfo stateProperty, PropertyInfo reducerInitialStateProperty, MethodInfo reducerReduceMethod, MethodInfo reducerEmptyPayloadReduceMethod)
            {
                Reducer = reducer;
                StateProperty = stateProperty;
                ReducerInitialStateProperty = reducerInitialStateProperty;
                ReducerReduceMethod = reducerReduceMethod;
                ReducerEmptyPayloadReduceMethod = reducerEmptyPayloadReduceMethod;
            }

            public ReducerBase Reducer { get; }
            public PropertyInfo StateProperty { get; }
            public PropertyInfo ReducerInitialStateProperty { get; }
            public MethodInfo ReducerReduceMethod { get; }
            public MethodInfo ReducerEmptyPayloadReduceMethod { get; }
        }

        private bool IsGenericReduceMethod(MethodInfo method, Type stateType)
        {
            return method.Name == c_reduceMethodName &&
                method.GetParameters().Length == 2 &&
                method.GetParameters()[0].ParameterType == stateType &&
                method.GetParameters()[1].ParameterType.IsGenericType && 
                method.GetParameters()[1].ParameterType.GetGenericTypeDefinition() == s_genericActionType;
        }

        private const string c_initialStatePropertyName = "InitialState";
        private const string c_reduceMethodName =  "Reduce";

        private static readonly Type s_genericActionType = typeof(Action<>);

        private List<ReducerConfig> m_reducerConfigs;
    }
}