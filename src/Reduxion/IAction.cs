namespace Reduxion
{
    public interface IAction
    {
        string Name { get; }
    }

    public interface IAction<T> : IAction
    {
        T Payload { get; }
    }
}