using System;
using System.Linq;
using System.Reflection;

namespace Reduxion
{
    public class Store<T> where T : new()
    {
        public T State { get; private set; }

        public Store(Reducer<T> reducer)
        {
            m_reducer = reducer;
            State = m_reducer.InitialState;
        }

        public Store(Reducer<T> reducer, T initialState)
        {
            m_reducer = reducer;
            State = initialState;
        }

        public void Dispatch<TState>(ActionCreator<TState> actionCreator) where TState : class, new()
        {
            var willReduceEventArgs = new WillReduceEventArgs<TState>(GetStateOrSubstateOfType<TState>(State));
            actionCreator.OnWillReduce(willReduceEventArgs);
            if (willReduceEventArgs.Cancel) {
                return;
            }

            m_reducer.Reduce(State, actionCreator.Fire());
            actionCreator.OnReduced(GetStateOrSubstateOfType<TState>(State));
        }

        public void Dispatch<TState, U, V>(ActionCreator<TState, U, V> actionCreator, V payload)
            where TState: class, new() 
            where U : class
        {
            var willReduceEventArgs = new WillReduceEventArgs<TState>(GetStateOrSubstateOfType<TState>(State));
            actionCreator.OnWillReduce(willReduceEventArgs);
            if (willReduceEventArgs.Cancel) {
                return;
            }

            m_reducer.Reduce(State, actionCreator.Fire(payload));
            actionCreator.OnReduced(GetStateOrSubstateOfType<TState>(State));
        }

        public void Dispatch(Action action)
        {
            m_reducer.Reduce(State, action);
        }

        public void Dispatch<U>(Action<U> action) where U : class
        {
            m_reducer.Reduce<U>(State, action);
        }

        private TState GetStateOrSubstateOfType<TState>(T topLevelState) where TState : class, new()
        {
            if (typeof(T) == typeof(TState))
            {
                return (TState)(topLevelState as object);
            }

            return GetFirstMatchingProperty<TState>(topLevelState);
        }

        private TState GetFirstMatchingProperty<TState>(object state) where TState : class, new()
        {
            var properties = state.GetType().GetProperties();
            {
                var property = properties.FirstOrDefault(x => x.PropertyType == typeof(TState));
                if (property != null)
                {
                    return (TState)property.GetValue(state);
                }
            }

            foreach (var property in properties)
            {
                object substate = property.GetValue(state);
                var matchingProperty = GetFirstMatchingProperty<TState>(substate);
                if (matchingProperty != null)
                {
                    return matchingProperty;
                }
            }

            return null;
        }

        private readonly Reducer<T> m_reducer;
    }
}