namespace Reduxion.Tests
{
    public class State
    {
        public UserState User { get; set; }
        public Config.State Config { get; set; }

        public override string ToString()
        {
            return $"{{\nUser: {User}\nConfig: {Config}\n}}";
        }
    }
}