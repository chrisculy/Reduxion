using System.Collections.Generic;
using Xunit;

namespace Reduxion.Tests
{
    public class ActionStateEventCallbackTests
    {
        class Main : Reducer<Main.State>
        {
            public class State
            {
                public int Value { get; set; }
            }

			public const int InitialStateValue = 14;

            public override State InitialState => new State { Value = InitialStateValue };

            public static ActionCreator<State, object, int> SetValue = ActionCreator<State, object, int>.Create();

            public State Reduce(State state, IAction<int> action)
            {
                if (action.Name == SetValue.Name)
                {
                    state.Value = action.Payload;
                }

                return state;
            }
		}

        [Fact]
        public static void TestEventOnMainState()
        {
            var reducer = new Main();
            var store = new Store<Main.State>(reducer);

            var setValueReducedCount = 0;
			System.Action<Main.State> onSetValueReduced = (Main.State state) => { setValueReducedCount++; };
			Main.SetValue.Reduced += onSetValueReduced;

			Assert.Equal(Main.InitialStateValue, store.State.Value);
            Assert.Equal(0, setValueReducedCount);

            store.Dispatch(Main.SetValue, 3);
            Assert.Equal(3, store.State.Value);
            Assert.Equal(1, setValueReducedCount);

			Main.SetValue.Reduced -= onSetValueReduced;
        }

        class Bottom : Reducer<Bottom.State>
        {
            public class State
            {
                public int Value { get; set; }
            }

			public const int InitialStateValue = 42;

			public override State InitialState => new State { Value = InitialStateValue };

            public static ActionCreator<State, object, int> SetValue = ActionCreator<State, object, int>.Create();

            public State Reduce(State state, IAction<int> action)
            {
                if (action.Name == SetValue.Name)
                {
                    state.Value = action.Payload;
                }

                return state;
            }
        }

        class Middle
        {
            public Bottom.State Bottom { get; set; }
        }

        class Top
        {
            public Middle Middle { get; set; }
        }

		[Fact]
        public static void TestEventOnNestedState()
        {
            var reducer = new CombinedReducer<Top>(new Dictionary<string, ReducerBase>()
            {
                {
                    "Middle",
                    new CombinedReducer<Middle>(new Dictionary<string, ReducerBase>()
                    {
                        { "Bottom", new Bottom() }
                    })
                },
            });
            var store = new Store<Top>(reducer);

            var setValueReducedCount = 0;
			System.Action<Bottom.State> onSetValueReduced = (Bottom.State state) => { setValueReducedCount++; };
			Bottom.SetValue.Reduced += onSetValueReduced;

            Assert.Equal(Bottom.InitialStateValue, store.State.Middle.Bottom.Value);
            Assert.Equal(0, setValueReducedCount);

            store.Dispatch(Bottom.SetValue, 17);
            Assert.Equal(17, store.State.Middle.Bottom.Value);
            Assert.Equal(1, setValueReducedCount);

			Bottom.SetValue.Reduced -= onSetValueReduced;
		}
    }
}