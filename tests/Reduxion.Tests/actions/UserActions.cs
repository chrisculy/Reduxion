namespace Reduxion.Tests
{
    public static class UserActions
    {
        public const string SET_NAME = "USER_SET_NAME";
        public const string SET_EMAIL = "USER_SET_EMAIL";
        public const string INCREASE_AGE = "USER_INCREASE_AGE";

        public static Action<string> SetName(string name)
        {
            return Action<string>.Create(SET_NAME, name);
        }

        public static Action<string> SetEmail(string email)
        {
            return Action<string>.Create(SET_EMAIL, email);
        }

        public static Action IncreaseAge()
        {
            return Action.Create(INCREASE_AGE);
        }
    }
}