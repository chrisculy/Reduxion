using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xunit;

namespace Reduxion.Tests
{
    public class IntegrationTests
    {
        [Fact]
        public static void ExampleTest()
        {
            var reducer = new CombinedReducer<State>(new Dictionary<string, ReducerBase>() {
                { "User", new UserReducer() },
                { "Config", new Config() }
            });
            var store = new Store<State>(reducer);

			var setEnvironmentReducedCount = 0;
			System.Action<Config.State> onSetEnvironment = (Config.State configState) => { setEnvironmentReducedCount++; };
			Config.SetEnvironment.Reduced += onSetEnvironment;

			var limitChangedCount = 0;
			System.Action<Config.State> onLimitChanged = (Config.State configState) => { limitChangedCount++; };
            Config.SetLimit.Reduced += onLimitChanged;
            Config.IncreaseLimit.Reduced += onLimitChanged;

            Config.IncreaseLimit.WillReduce += OnLimitWillChange;
			
            Assert.Equal(Environment.Development, store.State.Config.Environment);
            store.Dispatch(Config.SetEnvironment, Environment.Production);
            Assert.Equal(Environment.Production, store.State.Config.Environment);

            Assert.Equal(10, store.State.Config.Limit);
            store.Dispatch(Config.SetLimit, 11);
			Assert.Equal(11, store.State.Config.Limit);
            store.Dispatch(Config.IncreaseLimit);
			Assert.Equal(12, store.State.Config.Limit);

			// attempt to increase the limit past the bound enforced in OnLimitWillChange
            store.Dispatch(Config.IncreaseLimit);
			Assert.Equal(12, store.State.Config.Limit);

			Assert.Equal("", store.State.User.Name);
            store.Dispatch(UserActions.SetName("Frodo Baggins"));
			Assert.Equal("Frodo Baggins", store.State.User.Name);

			Assert.Equal("", store.State.User.Email);
			store.Dispatch(UserActions.SetEmail("frodo.baggins@middle.earth"));
			Assert.Equal("frodo.baggins@middle.earth", store.State.User.Email);

			Assert.Equal(0, store.State.User.Age);
            store.Dispatch(UserActions.IncreaseAge());
			Assert.Equal(1, store.State.User.Age);

			Config.SetEnvironment.Reduced -= onSetEnvironment;
            Config.SetLimit.Reduced -= onLimitChanged;
            Config.IncreaseLimit.Reduced -= onLimitChanged;
            Config.SetLimit.WillReduce -= OnLimitWillChange;
            Config.IncreaseLimit.WillReduce -= OnLimitWillChange;
        }

        [Fact]
        public void SingleReducerTest()
        {
            var reducer = new Config();
            var store = new Store<Config.State>(reducer);

			var setEnvironmentReducedCount = 0;
			System.Action<Config.State> onSetEnvironment = (Config.State configState) => { setEnvironmentReducedCount++; };
			Config.SetEnvironment.Reduced += onSetEnvironment;

			var limitChangedCount = 0;
			System.Action<Config.State> onLimitChanged = (Config.State configState) => { limitChangedCount++; };
            Config.SetLimit.Reduced += onLimitChanged;
            Config.IncreaseLimit.Reduced += onLimitChanged;

            Config.IncreaseLimit.WillReduce += OnLimitWillChange;
			
            Assert.Equal(Environment.Development, store.State.Environment);
            store.Dispatch(Config.SetEnvironment, Environment.Production);
            Assert.Equal(Environment.Production, store.State.Environment);

            Assert.Equal(10, store.State.Limit);
            store.Dispatch(Config.SetLimit, 11);
			Assert.Equal(11, store.State.Limit);
            store.Dispatch(Config.IncreaseLimit);
			Assert.Equal(12, store.State.Limit);

			// attempt to increase the limit past the bound enforced in OnLimitWillChange
            store.Dispatch(Config.IncreaseLimit);
			Assert.Equal(12, store.State.Limit);

			Config.SetEnvironment.Reduced -= onSetEnvironment;
            Config.SetLimit.Reduced -= onLimitChanged;
            Config.IncreaseLimit.Reduced -= onLimitChanged;
            Config.SetLimit.WillReduce -= OnLimitWillChange;
            Config.IncreaseLimit.WillReduce -= OnLimitWillChange;
        }

        private static void OnLimitWillChange(WillReduceEventArgs<Config.State> args)
        {
            if (args.State.Limit == 12)
            {
                args.Cancel = true;
                return;
            }
        }

        [Fact]
        public static void SerializationExampleTest()
        {
            var supportedActionTypeConfigs = new Dictionary<string, (Type Type, System.Action<Store<State>, object> Dispatch)>()
            {
                { typeof(string).Name, (typeof(Reduxion.Action<string>), (s, o) => s.Dispatch((Reduxion.Action<string>)o) ) }
            };

            Console.WriteLine("Serialization example:");

            var reducer = new CombinedReducer<State>(new Dictionary<string, ReducerBase>() {
                { "User", new UserReducer() },
                { "Config", new Config() }
            });
            var store = new Store<State>(reducer);

            var newName = "Peregrin Took";
            var setNameAction = UserActions.SetName(newName);
            var serializedAction = SerializedAction.Create(setNameAction);
            var serializedActionJson = JsonConvert.SerializeObject(serializedAction);

            var deserializedAction = JsonConvert.DeserializeObject<SerializedAction>(serializedActionJson);
            Assert.Equal(serializedAction, deserializedAction);

            var actionTypeConfig = supportedActionTypeConfigs[deserializedAction.Type];
            var deserializedStringAction = JsonConvert.DeserializeObject(deserializedAction.Action, actionTypeConfig.Type);

            Assert.Equal("", store.State.User.Name);
            actionTypeConfig.Dispatch(store, deserializedStringAction);
            Assert.Equal(newName, store.State.User.Name);
        }
    }
}