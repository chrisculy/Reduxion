using Newtonsoft.Json;
using System;

namespace Reduxion.Tests
{
    class SerializedAction
    {
        public static SerializedAction Create<T>(Action<T> action) where T : class
        {
            var serializedAction = new SerializedAction();
            serializedAction.Type = action.GetType().GenericTypeArguments[0].Name;
            serializedAction.Action = JsonConvert.SerializeObject(action);

            return serializedAction;
        }

        public string Type { get; set; }
        public string Action { get; set; }

        public override bool Equals(object obj)
        {
            var action = obj as SerializedAction;
			if (action == null)
			{
				return false;
			}

			return Type == action.Type && Action == action.Action;
        }

		public override int GetHashCode()
		{
			return HashCode.Combine(Type, Action);
		}
	}
}